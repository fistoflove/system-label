<?php

$fields = get_fields();

?>

<section class="image-text <?= $block['className'] ?>">
    <div class="container">
        <div class="wrapper">
            <div class="flex">
                <InnerBlocks/>
            </div>
            <div class="media">
                <?= imageElement($fields['content']['image']); ?>
            </div>
        </div>
    </div>
</section>