<?php

use Timber\Timber;
use Timber\Post;
use IMSWP\Helper\Scss;

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;

Timber::render( 'template.twig' , $context);
