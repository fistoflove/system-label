<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Content", "wysiwyg"],
        ["Items", "repeater", [
            ["Image", "image"],
            ["Label", "text"]
        ]],
    ]
);