<?php
/**
 * Child Starter functions and definitions
 *
 */

function imageElement($image, $size = false, $lazy = true, $cdn = true, $cdn_params = []) {
    $data = [
      "image" => $image,
      "size"  => $size,
      "lazy"  => $lazy,
      "cdn"   => $cdn,
      "cdn_params"   => $cdn_params
    ];

    Timber\Timber::render( 'image.twig', $data );
}

?>